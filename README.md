# Comandos Importantes

## Hacer build

```go
go build
```

## Ejecutar tras haber hecho build

```bash
./<nombre-ejecutable> # By default: ./cotan
```

## Hacer Build y Ejecutar

```go
go run .
```

## Comprobación de que el programa compila

```bash
task check
```

## Más documentación

- [Resto de documentación](./docs/README.md)
- [Instalar Go](https://go.dev/doc/install)
- [Instalar Task - Task Runner](https://taskfile.dev/installation/)
